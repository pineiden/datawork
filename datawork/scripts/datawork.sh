#!/bin/bash

### BEGIN INIT INFO
# Provides:          datawork
# Required-Start:    $syslog $local_fs
# Required-Stop:     $syslog $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:
# Short-Description: Datawork GNSS Process
### END INIT INFO

start(){
    /home/datawork/.virtualenvs37/datawork/bin/datawork
}

stop(){
    /bin/bash ./kill_datawork.h

}


case "$1" in 
    start)
       start
       ;;
    stop)
       stop
       ;;
    restart)
       stop
       start
       ;;
    status)
       # code to check status of app comes here 
       # example: status program_name
       ;;
    *)
       echo "Usage: $0 {start|stop|status|restart}"
esac


exit 0
